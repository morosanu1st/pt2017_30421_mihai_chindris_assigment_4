import java.time.LocalDateTime;

public class SavingAccount extends Account {
	int interest;
	
	public SavingAccount(Client holder, int balance, int period,Integer interest) {
		super(holder, balance, period);
		this.interest=interest;
	}
	public int withdraw(){
		this.setChanged();
		LocalDateTime now = LocalDateTime.now();
		now=now.minusDays(super.creationTime.getDayOfYear()).minusYears(super.creationTime.getYear());
		double temp=now.getYear()+now.getDayOfYear()/365;
		Message message=new Message(accountId,"Withdraw",(int)(ballance+ballance*temp*interest/100));
		this.notifyObservers(message);
		this.clearChanged();
		return (int)(ballance+ballance*temp*interest/100);
	}
	public String getAccountType(){
		return "Saving";
	}
	public int getInterest(){
		super.access("get interest");
		return interest;
	}
}
