import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public class Client implements Observer,Serializable{
	private static int lastId=1;
	private int clientId;
	private String name;
	private int phoneNumber;
	private int ballance;
	private ArrayList<Observer> observers;
	
	public Client(String name,int phoneNumber)
	{
		ballance=0;
		this.name=name;
		this.phoneNumber=phoneNumber;
		clientId=Client.lastId;
		Client.lastId++;
	}
	
	
	public int getBalance() {
		return ballance;
	}


	public void setBalance(int balance) {
		this.ballance = balance;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public int getPhoneNumber() {
		return phoneNumber;
	}
	
	public void setPhoneNumber(int phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	
	
	public String toString()
	{
		return "Name: "+name+", Phone Number: "+this.phoneNumber+"\n";
	}


	@Override
	public void update(Observable o, Object arg) {
		Message msg=(Message)arg;
		Account ac=(Account)o;
		String type=msg.getType();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
		if(type.equals("ChangeBy"))
		{
			int ammount=(int)msg.getInfo();
			ballance+=ammount;
			if(ammount<0)
				System.out.println(dtf.format(now)+" Client:"+this.name+"\n"+(-ammount)+" was withdrawn from the spending account "+msg.getId());
			else
				System.out.println(dtf.format(now)+" Client:"+this.name+"\n"+(ammount)+" was deposited to the spending account "+msg.getId());
			return;
		}
		if(type.equals("Withdraw"))
		{
			ballance-=ac.getBallance();
			int total=(int)msg.getInfo();
			System.out.println(dtf.format(now)+" Client:"+this.name+"\nThe sum "+total+"was withdrawn, closing the savings account: "+msg.getId());
		}
		if(type.equals("Access")){
			System.out.println(dtf.format(now)+" Client:"+this.name+"\nThe account with the id="+msg.getId()+" was accessed.\nType of access:"+(String)msg.getInfo());
		}
		
	}


}
