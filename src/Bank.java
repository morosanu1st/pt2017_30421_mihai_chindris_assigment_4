import java.io.Serializable;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JTable;

public class Bank implements Serializable,BankProc{
	 ArrayList<String> clientsName;
	 public ArrayList<String> getClientsName() {
		return clientsName;
	}

	public HashMap<String, Client> getClients() {
		return clients;
	}
	
	public HashMap<Client, ArrayList<Account>> getAccounts() {
		return accounts;
	}
	

	HashMap<String,Client> clients;
	 HashMap<Client,ArrayList<Account>> accounts;
	
	public Bank()throws AssertionError{
		clientsName=new ArrayList<String>();
		clients=new HashMap<String,Client>();
		accounts=new HashMap<Client,ArrayList<Account>>();
		assert isWellFormed():"is not well formed\n";
	}
	@Override
	public void restoreObservers(){
		for(String it:clientsName)
		{
			Client cl=clients.get(it);
			ArrayList<Account> temp=this.accounts.get(cl);
			for(Account ac:temp)
				ac.addObserver(cl);
		}
	}
	@Override
	public void addClient(String name, int phoneNumber)throws AssertionError {
		assert isWellFormed():"is not well formed\n";
		assert !clients.containsKey(name):"name already in use\n";
		assert !clientsName.contains(name):"name already in use\n";
		int size=clients.size();
		
		Client cl=new Client(name,phoneNumber);
		clientsName.add(name);
		clients.put(name,cl);
		accounts.put(cl,new ArrayList<Account>());
		
		assert clients.size()==size+1;
		assert clients.containsKey(name);
		assert isWellFormed():"is not well formed\n";
	}

	@Override
	public void removeClient(String name) throws AssertionError{
		assert isWellFormed():"is not well formed\n";
		assert clients.containsKey(name);
		int size=clients.size();
		
		Client temp=clients.get(name);
		clientsName.remove(name);
		accounts.remove(temp);
		clients.remove(name);
		
		assert !accounts.containsKey(temp);
		assert !clients.containsKey(name);
		assert clients.size()==size-1;
		assert isWellFormed():"is not well formed\n";
	}
	
	@Override
	public int createAccount(String holder, int balance, String type, int period,Integer interest) throws AssertionError{
		assert isWellFormed():"is not well formed\n";
		assert clients.containsKey(holder);
		assert balance>=0;
		assert type.equals("Saving")||type.equals("Spending");
		assert period>0;
		assert interest>=0;
		ArrayList<Account> temp=accounts.get(clients.get(holder));
		int size=temp.size();
		
		Client cl=clients.get(holder);
		Account ac;
		if(type.equals("Saving"))
			ac=new SavingAccount(cl,balance,period,interest);
		else
			ac=new SpendingAccount(cl,balance,period);
		temp.add(ac);
		cl.setBalance(cl.getBalance()+balance);
		
		assert size+1==temp.size();
		assert temp.contains(ac);
		assert isWellFormed():"is not well formed\n";
		return 0;
		
	}
	
	@Override
	public void deleteAccount(String holder,Account account) throws AssertionError{
		assert isWellFormed():"is not well formed\n";
		assert clients.containsKey(holder);
		Client c=clients.get(holder);
		ArrayList<Account> temp=accounts.get(c);
		assert temp.contains(account);
		
		c.setBalance(c.getBalance()-account.getBallance());
		temp.remove(account);
		
		assert !temp.contains(account);
		assert isWellFormed():"is not well formed\n";
	}
	
	@Override
	public JTable listAllClients() throws AssertionError{
		assert isWellFormed():"is not well formed\n";
		String[] header={"Name","Phone number","Ballance"};
		String[][] body=new String[clients.size()][3];
		Client temp;
		int i=0;
		for(String name:clientsName){
			temp=clients.get(name);
			body[i][0]=temp.getName();
			body[i][1]=Integer.toString(temp.getPhoneNumber());
			body[i][2]=Integer.toString(temp.getBalance());
			i++;
		}
		
		//?
		assert isWellFormed():"is not well formed\n";
		return new JTable((Object[][])body,(Object[])header);
	}
	@Override
	public JTable listAssociatedAccounts(String name)throws AssertionError{
		assert isWellFormed():"is not well formed\n";
		assert clients.containsKey(name);
		
		Client temp=clients.get(name);
		String[] header={"Id","Type","Ballance","Period","*Intrest","Status"};
		String[][] body=new String[accounts.get(temp).size()][6];
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy ");
		int i=0;
		String type;
		for(Account ac:accounts.get(temp)){
			type=ac.getAccountType();
			body[i][0]=Integer.toString(ac.getId());
			body[i][1]=type;
			body[i][2]=Integer.toString(ac.getBallance());
			body[i][3]=dtf.format(ac.getCreationTime())+" - "+dtf.format(ac.getCreationTime().plusMonths(ac.getPeriod()));
			if(type.equals("Saving"))
				body[i][4]=Integer.toString(((SavingAccount)ac).getInterest())+"%";
			else
				body[i][4]="N/A";
			if(ac.isActive())
				body[i][5]="Active";
			else
				body[i][5]="Inctive";
			i++;
		}
		assert isWellFormed():"is not well formed\n";
		JTable j=new JTable((Object[][])body,(Object[])header);
		j.getColumnModel().getColumn(3).setPreferredWidth(197);
		return j;
	}
	
	@Override
	public int modifyAccountBy(String holder,Account account, int ammount) throws AssertionError{
		assert isWellFormed():"is not well formed\n";
		assert clients.containsKey(holder);
		assert accounts.get(clients.get(holder)).contains(account);
		
		
		if(account.getAccountType().equals("Saving")){
			int temp1=((SavingAccount)account).withdraw();
			Client c=clients.get(holder);
			ArrayList<Account> temp=accounts.get(c);
			
			temp.remove(account);
			assert isWellFormed():"is not well formed\n";
			return temp1;
		}
		else
		{
			((SpendingAccount)account).setBallance(account.getBallance()+ammount);
			assert isWellFormed():"is not well formed\n";
			return account.getBallance();
		}
		
	}
	
	
	@Override
	public void changeHolderName(String holder, String newHolder)throws AssertionError{
		assert isWellFormed():"is not well formed\n";
		assert clients.containsKey(holder);
		assert !clients.containsKey(newHolder);
		
		clientsName.remove(holder);
		clientsName.add(newHolder);
		Client cl=clients.get(holder);
		cl.setName(newHolder);
		clients.remove(holder);
		clients.put(newHolder, cl);
		
		
		assert !clients.containsKey(holder);
		assert clients.containsKey(newHolder);
		assert isWellFormed():"is not well formed\n";
	}
	
	public boolean isWellFormed(){
		int acBalance=0;
		int clientBalance=0;
		Client cl;
		ArrayList<Account> tempList;
		for(String clName:clientsName){
			cl=clients.get(clName);
			clientBalance+=cl.getBalance();
			tempList=accounts.get(cl);
			for(Account ac :tempList)
				acBalance+=ac.getBallance();
		}
		
		return accounts.size()==clients.size()&&clients.size()==clientsName.size()&&acBalance==clientBalance;
	}
	
}
